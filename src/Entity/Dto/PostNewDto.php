<?php

declare(strict_types=1);

namespace App\Entity\Dto;

/**
 * DTO para almacenar los datos necesarios para crear un nuevo post.
 */
class PostNewDto
{
    private ?string $title = null;
    private ?string $body = null;
    private ?int    $author = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): PostNewDto
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): PostNewDto
    {
        $this->body = $body;

        return $this;
    }

    public function getAuthor(): ?int
    {
        return $this->author;
    }

    public function setAuthor(?int $author): PostNewDto
    {
        $this->author = $author;

        return $this;
    }
}
