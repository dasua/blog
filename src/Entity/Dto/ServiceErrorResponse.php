<?php

declare(strict_types=1);

namespace App\Entity\Dto;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * DTO para devolver errores a través de App\Helper\ServiceResponse.
 */
class ServiceErrorResponse
{
    #[Groups(['api_error_response'])]
    private int $code = Response::HTTP_BAD_REQUEST;
    #[Groups(['api_error_response'])]
    private string $message = '';
    #[Groups(['api_error_response'])]
    /**
     * @var string[][]
     */
    private array $errors = [];

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): ServiceErrorResponse
    {
        $this->code = $code;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): ServiceErrorResponse
    {
        $this->message = $message;

        return $this;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function addError(string $key, string $msg): self
    {
        $this->errors[$key][] = $msg;

        return $this;
    }

    public function addErrorsFromConstraintViolationList(ConstraintViolationList $errors): self
    {
        foreach ($errors as $error) {
            $this->addError($error->getPropertyPath(), $error->getMessage());
        }

        return $this;
    }
}
