<?php

declare(strict_types=1);

namespace App\Service\Post;

use App\Entity\Dto\PostNewDto;
use App\Entity\Dto\ServiceErrorResponse;
use App\Entity\Post;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NewPostService
{
    private Post $post;

    private function setData(PostNewDto $dto): void
    {
        $author = null === $dto->getAuthor() ? null : $this->authorRepository->find($dto->getAuthor());
        $this->post = new Post();
        $this->post->setTitle($dto->getTitle())
            ->setBody($dto->getBody())
            ->setAuthor($author);
    }

    private function validateData(): ConstraintViolationList
    {
        return $this->validator->validate($this->post);
    }

    public function __construct(private AuthorRepository $authorRepository, private ValidatorInterface $validator, private EntityManagerInterface $entityManager)
    {
    }

    public function __invoke(PostNewDto $dto): ServiceErrorResponse|Post
    {
        $this->setData($dto);
        $errors = $this->validateData();

        if (count($errors)) {
            $response = new ServiceErrorResponse();
            $response->setMessage('The sent data contains errors')
                ->setCode(Response::HTTP_BAD_REQUEST)
                ->addErrorsFromConstraintViolationList($errors);

            return $response;
        }

        $this->entityManager->persist($this->post);
        $this->entityManager->flush();

        return $this->post;
    }
}
