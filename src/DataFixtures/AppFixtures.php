<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    private array $authors = [];
    private Generator     $faker;
    private ObjectManager $manager;

    public function __construct()
    {
        $this->faker = Factory::create('es_ES');
    }

    private function addAuthors(): void
    {
        for ($i = 0; $i < $this->faker->numberBetween(8, 14); ++$i) {
            $author = new Author();
            $email = $this->faker->companyEmail();
            [$username,$domain] = explode('@', $email);
            $author->setName($this->faker->name())
                ->setEmail($email)
                ->setUsername($username)
            ;

            if ($this->faker->boolean(80)) {
                $author->setPhone($this->faker->phoneNumber());
            }

            if ($this->faker->boolean(70)) {
                $author->setWebsite(sprintf('https://www.%s', $domain));
            }

            if ($this->faker->boolean(70)) {
                $author->setStreet($this->faker->streetAddress())
                    ->setSuite($this->faker->words($this->faker->numberBetween(1, 3), true))
                    ->setCity($this->faker->city())
                    ->setZipcode($this->faker->postcode());
            }
            $this->manager->persist($author);
            $this->authors[] = $author;
        }
    }

    private function getPostBody(): string
    {
        $body = '';

        for ($i = 0; $i <= $this->faker->numberBetween(1, 5); ++$i) {
            $body .= $this->faker->paragraph($this->faker->numberBetween(1, 3)).PHP_EOL;
        }

        return trim($body);
    }

    private function addPosts(): void
    {
        for ($i = 0; $i <= $this->faker->numberBetween(12, 23); ++$i) {
            /** @var Author $author */
            $author = $this->faker->randomElement($this->authors);
            $post = new Post();
            $post->setTitle(ucwords($this->faker->words($this->faker->numberBetween(2, 5), true)))
                ->setBody($this->getPostBody())
                ->setAuthor($author);
            $this->manager->persist($post);
        }
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->addAuthors();
        $this->addPosts();
        $this->manager->flush();
    }
}
