<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Post;
use App\Repository\PostRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class PostListGetController extends AbstractFOSRestController
{
    #[Rest\Get(path: '/api/posts')]
    #[Rest\View(serializerGroups: ['api_post_list'], serializerEnableMaxDepthChecks: true)]
    /**
     * @OA\Response(
     *     response=200,
     *     description="Devuelve el listado de posts ordenados por fecha de creación descendente",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Post::class, groups={"api_post_list"}))
     *     )
     * )
     * @OA\Tag(name="Posts")
     */
    public function __invoke(PostRepository $postRepository): array|Response
    {
        return $postRepository->findAllByReverseCreated();
    }
}
