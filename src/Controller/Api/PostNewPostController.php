<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Dto\PostNewDto;
use App\Entity\Dto\ServiceErrorResponse;
use App\Entity\Post;
use App\Repository\PostRepository;
use App\Service\Post\NewPostService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PostNewPostController extends AbstractFOSRestController
{
    #[Rest\Post(path: '/api/posts/new')]
    #[Rest\View(serializerGroups: ['api_error_response', 'api_post_new'], serializerEnableMaxDepthChecks: true)]
    /**
     *  @OA\RequestBody(
     *     request="post",
     *     description="New Post data",
     *     @Model(type=PostNewDto::class)
     * )
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *        @Model(type=Post::class, groups={"api_post_new"})
     *     )
     * )
     * @OA\Response(
     *     response=400,
     *     description="Data Validation Errors",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=ServiceErrorResponse::class, groups={"api_error_response"}))
     *     )
     * )
     * @OA\Response(
     *     response=500,
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=ServiceErrorResponse::class, groups={"api_error_response"}))
     *     )
     * )
     * @OA\Tag(name="Posts")
     *
     * @throws ExceptionInterface
     */
    public function __invoke(PostRepository $postRepository, Request $request, SerializerInterface $serializer, NewPostService $newPostService): View
    {
        /** @var PostNewDto $dto */
        $dto = $serializer->deserialize($request->getContent(), PostNewDto::class, 'json');
        $result = ($newPostService)($dto);

        if ($result instanceof ServiceErrorResponse) {
            return View::create($result, $result->getCode());
        }

        $data = $serializer->normalize($result, null, [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
            'groups' => 'api_post_new',
        ]);

        return View::create($data, Response::HTTP_CREATED);
    }
}
