<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostListGetController extends AbstractController
{
    #[Route('/', name: 'app_homepage', methods: ['GET'])]
    public function index(PostRepository $postRepository): Response
    {
        $postList = $postRepository->findAllByReverseCreated();

        return $this->render('post_list_get/index.html.twig', [
            'post_list' => $postList,
        ]);
    }
}
