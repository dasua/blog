<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostGetController extends AbstractController
{
    #[Route('/post/{id}', name: 'app_post_get', methods: ['GET'])]
    public function index(Post $post): Response
    {
        return $this->render('post_get/index.html.twig', [
            'post' => $post,
        ]);
    }
}
