<?php

namespace App\Tests\Unit\Controller\Api;

use App\Controller\Api\PostListGetController;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @group Posts
 */
class PostListGetControllerTest extends KernelTestCase
{
    private EntityManagerInterface $em;

    protected function setUp(): void
    {
        parent::setUp();
        self::$kernel = self::bootKernel();
        $this->em = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->em->clear();
    }

    /**
     * @test
     */
    public function okPostListGetController(): void
    {
        $repo = $this->em->getRepository(Post::class);
        $controller = new PostListGetController();
        $result = ($controller)($repo);
        self::assertIsArray($result);
        self::assertArrayHasKey(0, $result);
        $post = reset($result);
        self::assertIsObject($post);
        self::assertInstanceOf(Post::class, $post);
    }
}
