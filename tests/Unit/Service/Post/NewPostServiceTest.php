<?php

namespace App\Tests\Unit\Service\Post;

use App\Entity\Author;
use App\Entity\Dto\PostNewDto;
use App\Entity\Dto\ServiceErrorResponse;
use App\Entity\Post;
use App\Repository\AuthorRepository;
use App\Service\Post\NewPostService;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

/**
 * @group Posts
 */
class NewPostServiceTest extends KernelTestCase
{
    private NewPostService $newPostService;
    private Generator $faker;
    private AuthorRepository $authorRepository;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $em = self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $this->authorRepository = $em->getRepository(Author::class);
        $this->newPostService = new NewPostService(
            authorRepository: $this->authorRepository,
            validator       : $validator,
            entityManager   : $em
        );
        $this->faker = Factory::create('es_ES');
    }

    /** @test */
    public function failsIfAuthorIsNull(): void
    {
        $dto = new PostNewDto();
        /** @var ServiceErrorResponse $result */
        $result = ($this->newPostService)($dto);
        self::assertInstanceOf(ServiceErrorResponse::class, $result);
        self::assertObjectHasAttribute('code', $result);
        self::assertIsInt($result->getCode());
        self::assertObjectHasAttribute('message', $result);
        self::assertIsString('message', $result->getMessage());
        self::assertNotEmpty($result->getMessage());
        self::assertObjectHasAttribute('errors', $result);
        $errors = $result->getErrors();
        self::assertIsArray($errors);
        self::assertArrayHasKey('author', $errors);
        self::assertNotEmpty($errors);
    }

    /** @test */
    public function failsIfAuthorDoesNotExits(): void
    {
        $dto = new PostNewDto();
        $dto->setAuthor(0);
        /** @var ServiceErrorResponse $result */
        $result = ($this->newPostService)($dto);
        self::assertInstanceOf(ServiceErrorResponse::class, $result);
        self::assertObjectHasAttribute('code', $result);
        self::assertIsInt($result->getCode());
        self::assertObjectHasAttribute('message', $result);
        self::assertIsString('message', $result->getMessage());
        self::assertNotEmpty($result->getMessage());
        self::assertObjectHasAttribute('errors', $result);
        $errors = $result->getErrors();
        self::assertIsArray($errors);
        self::assertArrayHasKey('author', $errors);
        self::assertNotEmpty($errors);
    }

    /** @test */
    public function failsIfTitleIsNull(): void
    {
        $dto = new PostNewDto();
        /** @var ServiceErrorResponse $result */
        $result = ($this->newPostService)($dto);
        self::assertInstanceOf(ServiceErrorResponse::class, $result);
        self::assertObjectHasAttribute('code', $result);
        self::assertIsInt($result->getCode());
        self::assertObjectHasAttribute('message', $result);
        self::assertIsString('message', $result->getMessage());
        self::assertNotEmpty($result->getMessage());
        self::assertObjectHasAttribute('errors', $result);
        $errors = $result->getErrors();
        self::assertIsArray($errors);
        self::assertArrayHasKey('title', $errors);
        self::assertNotEmpty($errors);
    }

    /** @test */
    public function failsIfTitleIsEmpty(): void
    {
        $dto = new PostNewDto();
        $dto->setTitle('');
        /** @var ServiceErrorResponse $result */
        $result = ($this->newPostService)($dto);
        self::assertInstanceOf(ServiceErrorResponse::class, $result);
        self::assertObjectHasAttribute('code', $result);
        self::assertIsInt($result->getCode());
        self::assertObjectHasAttribute('message', $result);
        self::assertIsString('message', $result->getMessage());
        self::assertNotEmpty($result->getMessage());
        self::assertObjectHasAttribute('errors', $result);
        $errors = $result->getErrors();
        self::assertIsArray($errors);
        self::assertArrayHasKey('title', $errors);
        self::assertNotEmpty($errors);
    }

    /** @test */
    public function failsIfBodyIsNull(): void
    {
        $dto = new PostNewDto();
        /** @var ServiceErrorResponse $result */
        $result = ($this->newPostService)($dto);
        self::assertInstanceOf(ServiceErrorResponse::class, $result);
        self::assertObjectHasAttribute('code', $result);
        self::assertIsInt($result->getCode());
        self::assertObjectHasAttribute('message', $result);
        self::assertIsString('message', $result->getMessage());
        self::assertNotEmpty($result->getMessage());
        self::assertObjectHasAttribute('errors', $result);
        $errors = $result->getErrors();
        self::assertIsArray($errors);
        self::assertArrayHasKey('body', $errors);
        self::assertNotEmpty($errors);
    }

    /** @test */
    public function failsIfBodyIsEmpty(): void
    {
        $dto = new PostNewDto();
        $dto->setBody('');
        /** @var ServiceErrorResponse $result */
        $result = ($this->newPostService)($dto);
        self::assertInstanceOf(ServiceErrorResponse::class, $result);
        self::assertObjectHasAttribute('code', $result);
        self::assertIsInt($result->getCode());
        self::assertObjectHasAttribute('message', $result);
        self::assertIsString('message', $result->getMessage());
        self::assertNotEmpty($result->getMessage());
        self::assertObjectHasAttribute('errors', $result);
        $errors = $result->getErrors();
        self::assertIsArray($errors);
        self::assertArrayHasKey('body', $errors);
        self::assertNotEmpty($errors);
    }

    /** @test */
    public function okIfPostDataIsCorrect(): void
    {
        $dto = new PostNewDto();
        /** @var Author $author */
        $author = $this->authorRepository->findBy([], null, 1)[0];
        $dto->setTitle($this->faker->words(3, true))
            ->setBody($this->faker->paragraph(6))
            ->setAuthor($author->getId());
        /** @var Post $post */
        $post = ($this->newPostService)($dto);
        self::assertInstanceOf(Post::class, $post);
        self::assertObjectHasAttribute('id', $post);
        self::assertIsInt($post->getId());
    }
}
