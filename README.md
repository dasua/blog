# Software que debe estar instalado

- Docker
- docker-compose
- symfony-cli
- composer
- PHP 8.0

# Clonación del repositorio y comprobaciones iniciales

```shell
git clone git@gitlab.com:dasua/blog.git \
&& cd blog \
&& composer check \
&& composer install
```

# Inicio del servicio en entornos de desarrollo

```shell
docker-compose up -d
symfony serve -d
bin/console doctrine:migrations:migrate
bin/console doctrine:fixtures:load
```

## Inicialización para la ejecución de test

```shell
bin/console doctrine:database:create --env=test
bin/console doctrine:migrations:migrate --env=test
bin/console doctrine:fixtures:load --env=test
```

# Paquetes instalados

Los más comunes para un desarrollo de este tipo como pueden ser:

- validator
- twig
- logger
- orm
- symfony/asset
- sensio/framework-extra-bundle
- symfony/serializer-pack

## Solo dev

- symfony/maker-bundle
- friendsofphp/php-cs-fixer
- phpunit
- orm
- orm-fixtures
- symfony/profiler-pack

Además, se han instalado los siguientes:

- **nelmio/api-doc-bundle**: documentación de APIS
- **friendsofsymfony/rest-bundle**: herramientas para desarrollo de RESTfull APIS
- **dama/doctrine-test-bundle**: solo en entorno dev. Para hacer rollback automático en los test

# Ejecución de los test

Los test realizados están en el mismo grupo Posts.

```shell
bin/phpunit --group=Posts --testdox
```

# Parada del entorno de desarrollo

```shell
docker-compose down
symfony serve:stop
```

# Observaciones sobre la prueba

## FOSRest Bundle

Al tener que implementar un API y mejor no utilizar API Platform, he optado por utilizar [ FriendsOfSymfony /
FOSRest Bundle](https://github.com/FriendsOfSymfony/FOSRestBundle) y así no tener que implementar toda la gestión del API
de forma manual.

Al no haberlo utilizado nunca, me encontré con las dificultades típicas de cómo hacer las cosas con dicho bundle.

## NelmioApiDoc Bundle

Para la documentación del API, opté por [NelmioApiDoc Bundle](https://symfony.com/bundles/NelmioApiDocBundle/current/index.html)
ya que permite generar toda la documentación con anotaciones en las entidades, controladores, etc.

Si bien lo había utilizado en la versión para con Symfony 4.4, la nueva versión tiene más características (e incompatibilidades).
De hecho, al principio no me funcionaba bien porque estaba utilizando los atributos de PHP 8.0 y parece ser que no está terminado
de implementar con lo que al final tuve que utilizar las anotaciones.

En el blog, hay un enlace para acceder a la documentación del API.

## Doctrine Test Bundle

Una vez configurado, permite ejecutar los test sin que la base de datos se vea modificada (más allá de la modificación de las secuencias).

## Datos de prueba

He utilizado orm-fixtures para generar datos aleatorios en ambas bases de datos (dev y test).

A la hora de preparar los datos he procurado que sean algo aleatorios (faltando datos opcionales, distinto número de inserciones, etc).

# Implementación de la parte frontend

Los controladores solo tienen un método que responde a un endpoint (index para mantener el nombre que propone Symfony).

Al tener muy poca lógica, no lo he implementado con servicios.

# Implementación de la parte backend

También en este caso los controladores solo responden a un `endpoint + method` y el método siempre es `__invoke`.

El controlador `PostListGetController` hace referencia a Post List (listado de post), con el METHOD GET.

No lo he implementado con servicio ya que es solo una llamada al repositorio.

El controlador `PostNewPostController` hace referencia a añadir un nuevo post, con el método POST.

En esta ocasión sí que se ha implementado como servicio.

El controlador recibe los datos, los pasa a un DTO el cual se pasa como parámetro al __invoke del servicio.

`App\Service\Post\NewPostService` es el encargado de validar los datos y devolver los errores o bien realizar la inserción
en base de datos.

# Actualización de datos de auditoría de las entidades

Como datos de auditoría únicamente he utilizado createdAt y updatedAt.

Para no setearlos manualmente, he creado `App\Traits\TimestampableEntity` utilizando los hacks que proporciona Doctrine y
así que se gestionen automáticamente.

# php-cs-fixer

He configurado esta herramienta para que se pueda dar estilo de forma automática al código.

Por un lado, he añadido un script a composer para poder lanzarlo manualmente:

```shell
composer fix-code-style
```

Tomará la configuración de `.php-cs-fixer.dist.php` y la aplicará sobre la carpeta `src/`.

También he configurado el PhpStorm para que tome dicha configuración y añadido un File Watcher para que al guardar el
fichero se ejecute automáticamente la herramienta con dicha configuración.
