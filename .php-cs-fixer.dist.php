<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude(['var', 'vendor'])
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PSR2' => true,
        '@Symfony' => true,
        'array_indentation' => true,
        'yoda_style' => true,
        'trailing_comma_in_multiline' => [
            'elements' => ['arrays'],
        ],
        'blank_line_before_statement' => [
            'statements' => ['for', 'foreach', 'if', 'return', 'switch', 'while', 'yield'],
        ],
        'method_chaining_indentation' => true,
    ])
    ->setFinder($finder)
;
